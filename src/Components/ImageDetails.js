import React from 'react'
import {Text, StyleSheet, Image, View} from 'react-native'

const ImageDetails = ({image, title, score}) => {
    return (
       <View>
       <Image source={image} />
        <Text style={styles.textStyle}>Name: {title}</Text>
        <Text style={styles.textStyle}>Score: {score}</Text>
       </View>
    );
};

const styles= StyleSheet.create({
    textStyle:{
        fontSize:15,
        marginLeft:5
    }
});

export default ImageDetails;