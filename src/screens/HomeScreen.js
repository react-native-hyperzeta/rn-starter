import React from "react";
import { Text, View, StyleSheet, Button, TouchableOpacity } from "react-native";

const HomeScreen = ({navigation}) => {
  return (
    <View>
      <Text style={styles.text}>Hello world !</Text>
      <Button
        title="Practice1"
        onPress={() => navigation.navigate('Practice1')}
      />
      <TouchableOpacity onPress={() => navigation.navigate('List')}>
        <Text>TouchableOpacity</Text>
      </TouchableOpacity>
      <Button
      title="ImageScreen"
      onPress={() => navigation.navigate('Image')}
      />
      <Button
        title='Counter'
        onPress={()=> navigation.navigate('Counter')}
      />
      <Button
      title="ColorScreen"
      onPress={() => navigation.navigate('Color')}
      />
      <Button
        title='SquarScreen'
        onPress={()=>navigation.navigate('Square')}
      />
      <Button
        title="SquareScreenReducer"
        onPress={()=>navigation.navigate('SquareReducer')}
      />
      <Button
        title="TextInput"
        onPress={()=>navigation.navigate('TextInput')}
      />
      <Button
        title = 'LayoutScreen'
        onPress = {()=>navigation.navigate('Layout')}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  text: {
    fontSize: 30
  }
});

export default HomeScreen;
