import React, {useState} from 'react';
import {Text, View, StyleSheet, TextInput} from 'react-native';

const TextInputScreen = ()=> {
    const [name, setName] = useState('');

    return (
        <View>
            <Text>Enter Name: </Text>
            <TextInput style={styles.input}
                autoCapitalize = 'none'
                autoCorrect = {false}
                value={name}
                onChangeText={(newValue)=>setName(newValue)}
            />
            {(name.length<5) ? <Text>Name must be at least 5 characters</Text> : <Text>Great!</Text>}
            <Text style={styles.text}>My name is: {name}</Text>
        </View>
    )
};

const styles=StyleSheet.create({
    input: {
        margin: 15,
        borderWidth: 1,
        borderColor: 'black',
    },
    text: {
        fontSize: 25,
        marginLeft: 20,
        marginTop:20,
    }
});

export default TextInputScreen;