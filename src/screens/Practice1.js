import React from 'react'
import {Text, View, StyleSheet } from 'react-native'

const Practice1 =()=> {
    const myName="Karunakaran";

    return (
        <View>
            <Text style={styles.firstText}>Getting started with Reac-Native</Text>
            <Text style={styles.secondText}>My name is {myName}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    firstText: {
        fontSize: 40
    },
    secondText: {
        fontSize: 30
    }
});

export default Practice1;