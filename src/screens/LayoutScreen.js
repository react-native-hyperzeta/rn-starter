import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

const LayoutScreen = ()=> {
    return(
        <View style={styles.parentViewStyle}>
           <View style={styles.viewOneStyle}></View>
           <View style={styles.viewTwoStyle}></View>
           <View style={styles.viewThreeStyle}></View>
        </View>
    )
};

const styles = StyleSheet.create({
    parentViewStyle: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 200,
        borderWidth: 5,
        borderColor: 'pink',
        padding: 10,
        margin: 15
    },
    viewOneStyle: {
        height: 50,
        width: 50,
        backgroundColor: 'red'
    },
    viewTwoStyle: {
        height: 50,
        width: 50,
        backgroundColor: 'green',
        alignSelf: 'flex-end',
    },

    viewThreeStyle: {
        height: 50,
        width: 50,
        backgroundColor: 'blue'
    }
});

export default LayoutScreen;