import React from 'react';
import {Text, View, StyleSheet} from 'react-native';
import ImageDetails from '../Components/ImageDetails'

const ImageScreen = () => {
    return(
       <View>
            <ImageDetails title="Forest" image={require('../../assets/forest.jpg')} score={5} />
            <ImageDetails title="Beach" image={require('../../assets/beach.jpg')} score={3} />
            <ImageDetails title="Mountain" image={require('../../assets/mountain.jpg')} score={7} />
       </View>
    );
};

const styles = StyleSheet.create({

});

export default ImageScreen;